import logo from './logo.svg';
import './App.css';
import {Component} from 'react';

// function App() {
//   return (
//     <div className="App">
//      hello react!
//     </div>
//   );
// }

class App extends Component{
  render(){
    return(
      <div className="App">
        hello react!
      </div>
    )
  }
}

export default App;
